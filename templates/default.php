<?php 
// Put the categories together into one string
foreach ($categories as $c) {
  $cat .= "#$c ";
}
?>
                <div class="col-lg-4 col-sm-6">
                    <div class="portfolio-box">
                        <a href="<?=$link?>">
                            <?=$thumb?>
                            <div class="portfolio-box-caption">
                                <div class="portfolio-box-caption-content">
                                    <div class="project-name">
                                        <?=$title?>
                                    </div>
                                    <div class="project-category text-faded">
                                        <?=$cat?>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

<?php $cat = ''; // Clear out the category list ready for the next item ?>
