# Introduction

This plugin allows you to create portfolio posts and insert a gallery of them
into your pages using simple shortcodes.


# Portfolio Posts

The plugin create a custom post type called Portfolio. The content of the Portfolio
posts are just like any other post, but there are some extra features:

* Weight: an integer number which detirmines where a post appears in a gallery. The higher the weigth, the lower down it goes.
* Categories: a number of tags which can be displayed in a template
* Gallery: the galleries that the post will appear for


# Shortcode

To display a grid of Portfolio posts insert a shortcode similar to this:

    [portfolio-tiles gallery="name" template="default" maxtiles="6"]

_gallery_ must be specified - all Portfolio posts that have been added to this gallery will be used

The featured image of the Portfolio posts will be displayed along with the title and categories (if set in the template)


# Templates

HTML template files can be stored in these locations (in order of precedence):

* wp-content/portfolio-tiles-templates
* <active-theme>/portfolio-tiles-templates (works best with child themes)
* wp-content/plugins/portfolio-tiles/templates (these are included with the plugin, it isn't recommended to change these)

The following PHP variables can be used inside the templates:

* $thumb - the thumbnail image
* $title - the post title
* $link - the link to the post
* $categories - an array of the categories
