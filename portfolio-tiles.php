<?php
/*
Plugin Name: Portfolio Tiles
Plugin URI:  https://developer.wordpress.org/plugins/
Description: Add portfolio items and display in a grid using shortcode
Version:     0.2
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );


/*
 * Create custom taxonomy
 */
add_action( 'init', 'pt_create_my_taxonomies', 0 );

function pt_create_my_taxonomies() {
    // Categories
    register_taxonomy(
        'portfolio_category',
        'portfolio',
        array(
            'labels' => array(
                'name' => 'Portfolio Category',
                'add_new_item' => 'Add New Portfolio Category',
                'new_item_name' => "New Portfolio Category"
            ),
            'show_ui' => true,
            'show_tagcloud' => false,
            'hierarchical' => true
        )
    );
    
    // Gallery names
    register_taxonomy(
        'portfolio_gallery',
        'portfolio',
        array(
            'labels' => array(
                'name' => 'Portfolio Gallery Name',
                'add_new_item' => 'Add New Portfolio Gallery Name',
                'new_item_name' => "New Portfolio Gallery Name"
            ),
            'show_ui' => true,
            'show_tagcloud' => false,
            'hierarchical' => false
        )
    );
}

/*
* Creating a function to create our CPT
*/

function pt_custom_post_type() {
 
// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Portfolio', 'Post Type General Name', 'twentythirteen' ),
        'singular_name'       => _x( 'Portfolio', 'Post Type Singular Name', 'twentythirteen' ),
        'menu_name'           => __( 'Portfolio', 'twentythirteen' ),
        'parent_item_colon'   => __( 'Parent Portfolio', 'twentythirteen' ),
        'all_items'           => __( 'All Portfolios', 'twentythirteen' ),
        'view_item'           => __( 'View Portfolio', 'twentythirteen' ),
        'add_new_item'        => __( 'Add New Portfolio', 'twentythirteen' ),
        'add_new'             => __( 'Add New', 'twentythirteen' ),
        'edit_item'           => __( 'Edit Portfolio', 'twentythirteen' ),
        'update_item'         => __( 'Update Portfolio', 'twentythirteen' ),
        'search_items'        => __( 'Search Portfolio', 'twentythirteen' ),
        'not_found'           => __( 'Not Found', 'twentythirteen' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'twentythirteen' ),
    );
     
// Set other options for Custom Post Type
     
    $args = array(
        'label'               => __( 'portfolio', 'twentythirteen' ),
        'description'         => __( 'Portfolio items', 'twentythirteen' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'pt_custom_attachment', 'thumbnail', 'description', 'comments', 'revisions', 'categories', 'tags' ),
        // You can associate this CPT with a taxonomy or custom taxonomy. 
        'taxonomies'          => array( '' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */ 
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
    );

    // Registering your Custom Post Type
    register_post_type( 'portfolio', $args ); 
}
 
/* Hook into the 'init' action so that the function
* Containing our post type registration is not 
* unnecessarily executed. 
*/
 
add_action( 'init', 'pt_custom_post_type', 0 );

// Add support for featured images
add_theme_support( 'post-thumbnails' );



// Portfolio gallery shortcode
function pt_show_portfolio( $atts ){

    // Set the default arguments
    $args = shortcode_atts( array(
        'template' => 'default',
        'gallery' => '',
        'maxtiles' => 6
    ), $atts );
    
    $maxTiles = $args['maxtiles'];
    $galleryName = $args['gallery'];
    $templateName = $args['template'];

    // Get the gallery details
    $gallery = get_term_by('name', $galleryName, 'portfolio_gallery');
    
    // Start going through the tiles and writing them out
    $output = '<div class="container-fluid">
            <div class="row no-gutter">';

    $loop = new WP_Query( array( 'post_type' => 'portfolio',
                                 'tax_query' => array(
                                    array(
                                        'taxonomy'  => 'portfolio_gallery',
                                        'field'     => 'name',
                                        'terms'     => $galleryName
                                        )
                                    ),
                                 'paged' => $paged, 
                                 'post_status' => 'publish',
                                 'meta_key' => 'pt_weight',
                                 'orderby' => 'meta_value_num',
                                 'order' => 'ASC' ) );
    if ( $loop->have_posts() ) {
        while ( $loop->have_posts() && $tileCount < $maxTiles ) { 
		$loop->the_post();
		// Get the title
		$title = get_the_title();
		
		// Get the thumbnail image
		$thumb = get_the_post_thumbnail();
		
		// Get the list of categories
		$terms = get_the_terms($post->ID, 'portfolio_category');
		
		// Get link to the post;
		$link = get_permalink();

		// Go through the categories
    $categories = array();
		if (count($terms) > 0) {
        foreach ($terms as $t) {
            $categories[] = $t->name;
        }
    }

    // Get the template name and path
    if (file_exists(WP_CONTENT_DIR . "/portfolio-tiles-templates/$templateName.php")) {
      $template = WP_CONTENT_DIR . "/portfolio-tiles-templates/$templateName.php"; // First look in the wp-content dir
    } elseif (file_exists(get_template_directory() . "/portfolio-tiles-templates/$templateName.php")) {
      $template = get_template_directory() . "/portfolio-tiles-templates/$templateName.php"; // Next look in the theme
    } elseif (file_exists(__DIR__ . "/templates/$templateName.php")) {
      $template = __DIR__ . "/templates/$templateName.php"; // Templates included with the plugin
    }

    // Start processing the template
    ob_start();
    include($template);
    $output .= ob_get_clean();
		//$output .= "Title: $title <br />Pic: $thumb <br />";
		
    $categories = array(); // Reset var

    // Increment the tile count
    $tileCount++;
        }
    }
    wp_reset_postdata();

	$output .= '	</div>
		</div>';
	return $output;
}
add_shortcode( 'portfolio-tiles', 'pt_show_portfolio' );


// Add custom content field to portfolio content type
add_action("admin_init", "pt_admin_init");
 
function pt_admin_init(){
  add_meta_box("portfolio_post_weight", "Post Weight", "pt_weight", "portfolio", "side", "low");
}
 
function pt_weight(){
  global $post;
  $custom = get_post_custom($post->ID);
  $pt_weight = $custom["pt_weight"][0];
  ?>
  The lower the number the higher the portfolio will be
  <label>Weight:</label>
  <input name="pt_weight" value="<?=$pt_weight?>" />
  <?php
}

add_action('save_post', 'pt_save_details');

function pt_save_details(){
  global $post;
 
  update_post_meta($post->ID, "pt_weight", $_POST["pt_weight"]);
}
